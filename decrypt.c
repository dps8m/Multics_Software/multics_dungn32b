#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define RECORDS 3391
#define RECLEN 80
    
int main (int argc, char * argv [])
  {
    int fd_in = open ("DTEXT.DAT", O_RDONLY, 0);
    if (fd_in < 0)
      {
        perror ("opening DTEXT.DAT");
        exit (1);
      }

    FILE * f_out = fopen ("DTEXT.TXT", "w");
    if (! f_out)
      {
        perror ("opening DTEXT.TXT");
        exit (1);
      }


    int lineno = 0;
    while (1)
      {
        unsigned char buf[RECLEN];
        char outbuf[RECLEN - 4 + 1];
        outbuf[RECLEN - 4] = 0;
        int rc = read (fd_in, buf, sizeof (buf));
        if (rc == 0)
          break;
        if (rc != sizeof (buf))
          {
            perror ("reading DTEXT.DAT");
            exit (1);
          }
        lineno ++;
        int32_t key = * (int32_t *) buf;
        if (key == -1)
          {
            for (int c = 4; c < RECLEN; c ++)
              {
                outbuf[c -4] = buf[c];
              }
          }
        else
          {
            for (int c = 4; c < RECLEN; c ++)
              {
                unsigned int x = (lineno & 31) + c - 3;
                outbuf[c -4] = buf[c] ^ x;
              }
          }
        fprintf (f_out, "%4d%s\n", key, outbuf);
      } // while (1)

    close (fd_in);
    fclose (f_out);
    return 0;
  }
